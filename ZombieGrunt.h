// ZombieGrunt.h

#pragma once

#include "GameEntity.h"
#include "Weapon.h"

class ZombieGrunt : public GameEntity
{
private:
	Weapon*	currentWeapon;

public:
	ZombieGrunt() :
		GameEntity(),
		currentWeapon(new Weapon())
	{
		std::cout << "Zombie Grunt Created" << std::endl;
	}

	~ZombieGrunt()
	{
		std::cout << "Zombie Grunt Destroyed" << std::endl;
	}

	void takeDamage(float damage)
	{
		health -= damage;
		std::cout << "Zombie Grunt Takes Damage" << std::endl;
	}

	void move(const Vector3& delta)
	{
		position = position + delta;
	}
};
