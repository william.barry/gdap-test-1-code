// GameEntity.h

#pragma once

#include <iostream>
#include "Vector3.h"

class GameEntity
{
protected:
 Vector3  position;
 int   health;
 Vector3  rotation;

public:
 GameEntity() :
  position(),
  health(100),
  rotation()
 {
  std::cout << "Game Entity Created" << std::endl;
 }

 ~GameEntity()
 {
  std::cout << "Game Entity Destroyed" << std::endl;
 }

 void takeDamage(float damage)
 {
  health -= damage;
  if (health <= 10)
  {
   std::cout << "Game Entity LOW HEALTH" << std::endl;
  }
 }

 virtual void move(const Vector3& delta) = 0;
};
