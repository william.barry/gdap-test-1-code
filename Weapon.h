// Weapon.h

#pragma once

#include <iostream>

class Weapon
{
private:
	float		damage;

public:
	Weapon() :
		damage(100)
	{
		std::cout << "Weapon Ready!" << std::endl;
	}

	float getDamage() { return damage; }
};
