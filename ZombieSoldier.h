// ZombieSoldier.h

#pragma once

#include "GameEntity.h"

class ZombieSoldier : public GameEntity
{
private:
	int			ammo;

public:
	ZombieSoldier() :
		GameEntity(),
		ammo(100)
	{
		std::cout << "Zombie Soldier Created" << std::endl;
	}

	~ZombieSoldier()
	{
		std::cout << "Zombie Soldier Destroyed" << std::endl;
	}

	void takeDamage(float damage)
	{
		health -= damage;
		std::cout << "Zombie Soldier LOW HEALTH" << std::endl;
	}

	void move(const Vector3& delta)
	{
		position = position + delta;
	}
};
