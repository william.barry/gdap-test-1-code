// Vector3.h

#pragma once

class Vector3 
{
public:
    Vector3() : 
  x(0), y(0), z(0)
    {}

    Vector3(float _x, float _y, float _z) : 
  x(_x), 
  y(_y),
  z(_z)
    {}

 void setPoint(float _x, float _y, float _z) { x = _x; y = _y; z = _z; }

 Vector3 operator +(const Vector3& _right) {
  return Vector3(x + _right.x, y + _right.y, z + _right.z);
 }

public:
    float x, y, z;
};
