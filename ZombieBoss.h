#pragma once

class ZombieBoss
{
private:
    std::string name;

public:
    ZombieBoss(const char* _name) :
        name(_name)
    {
    }

    ~ZombieBoss()
    {
    }

    void ChangeName(const char* _name)
    {
        name = _name;
    }

    void DisplayName()
    {
        std::cout << "Zombie: " << name.c_str() << std::endl;
    }
};
